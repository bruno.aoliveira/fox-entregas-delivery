import { Switch, Route, BrowserRouter} from "react-router-dom";
import { PrivateRoute } from "./components/PrivateRoute";
import { PublicRouteOnly } from "./components/PublicRouteOnly";
import { HomeView } from "./views/Home";
import { LoginView } from "./views/Login";
import { NewOrderView } from "./views/NewOrder";
import { NotFound } from "./views/NotFound";
import { RegisterView } from "./views/Register";

export const Routes = () => {
  return(
    <BrowserRouter>
      <Switch>
        <Route path="/" exact>
          <HomeView />
        </Route>
        <PublicRouteOnly path="/cadastro" exact>
          <RegisterView />
        </PublicRouteOnly>
        <PublicRouteOnly path="/login" exact>
          <LoginView />
        </PublicRouteOnly>
        <PrivateRoute path="/novo-pedido" exact>
          <NewOrderView />
        </PrivateRoute>
        <Route path="*">
          <NotFound />
        </Route>
      </Switch>
    </BrowserRouter>
  )
}
import { Alert, Col, Container, Form, Row } from 'react-bootstrap'
import { Layout } from '../../components/Layout'
import { PageTitle } from '../../components/PageTitle'
import { FormField } from '../../components/FormField'
import { Button } from '../../components/Button'
import { useFormik } from "formik";
import * as yup from "yup";
import { IMaskInput }  from "react-imask";
import { createUser } from '../../services/createUser'
import { useState } from 'react'
import {  Link, useHistory } from 'react-router-dom'
import { getErrorDescription } from '../../services/getErrorDescription'

export const RegisterView = () => {

  const [generalErrors, setGeneralErrors] = useState();
  const history = useHistory();

  const formik = useFormik({
    initialValues: {
      name: "",
      email: "",
      phone: "",
      password: "",
      agree: false
    },
    validationSchema: yup.object().shape({
      name: yup.string().required("Por favor, preencha o nome.")
      .min(5, "Informe pelo menos 5 caracteres."),
      email: yup.string().required("Por favor, preencha o e-mail.")
      .email("Por favor, preencha um e-mail válido."),
      phone: yup.string().required("Por favor, preencha o telefone."),
      password: yup.string().required("Por favor, preencha a senha.")
      .min(8, "Informe a senha com pelo menos 8 caracteres.")
      .max(50, "Informe a senha com no máximo 50 caracteres."),
      agree: yup.boolean().equals([true], "É necessário aceitar os termos.")
    }),
    onSubmit: async (values, {setFieldError}) => {
      const {error, data} = await createUser(values)
      if(!error){
        history.push("/")
        return;
      }
      if(data.msg === "ValidationError"){
        data.errors.forEach( error => {
          const message = getErrorDescription(error.msg)
          setFieldError(error.param, message)
        })
        return;
      }
      setGeneralErrors("Ocorreu um erro ao cadastrar, tente novamente!")
    }
  })

  const getFieldProps = fieldName => {
    return {
      ...formik.getFieldProps(fieldName),
      error:  formik.errors[fieldName] ,
      isInvalid:  formik.touched[fieldName] && formik.errors[fieldName] ,
      isValid:  formik.touched[fieldName] && !formik.errors[fieldName]
    }
  }
  return (
    <Layout>
      <Container>
        <Row className="justify-content-center">
          <Col lg={6}>
            <PageTitle>Nova Conta</PageTitle>
            <Form onSubmit={formik.handleSubmit}>
              <FormField
              {...getFieldProps("name")}
                label='Nome'
                placeholder='Nome'
              />
              <FormField
              {...getFieldProps("email")}
                label='E-mail'
                placeholder='Ele será o seu usuário'
                type='email'
              />
              <FormField
              {...getFieldProps("phone")}
                label='Telefone'
                placeholder='(00) 00000-0000'
                as={IMaskInput}
                mask={[
                  { mask: '(00) 0000-0000' },
                  { mask: '(00) 00000-0000' }
                ]}
                onChange={undefined}
                onAccept={value => formik.setFieldValue("phone", value)}
              />
              <FormField
              {...getFieldProps("password")}
                label='Senha'
                placeholder='Informe sua senha de acesso'
                type='password'
              />
              <Form.Check
              {...formik.getFieldProps("agree")}
              type="checkbox"
                label={<span>Eu li e aceito os <a href='/termos-de-uso.pdf' target='_blank'>Termos de Uso</a>.</span>}
              />
              {formik.touched.agree && formik.errors.agree && (
                <Form.Control.Feedback type='invalid' className="d-block">
                {formik.errors.agree}
              </Form.Control.Feedback>
              )}
              {generalErrors && (
                <Alert variant='danger'>
                  {generalErrors}
                </Alert>
              )}
              <Button
                type="submit"
                block
                className='mt-3 mb-4'
                disabled={formik.isValidating || formik.isSubmitting}
                loading={formik.isValidating || formik.isSubmitting}
              >
                Criar conta
              </Button>
              <p className='text-center'>
                Já possui conta?
                <Link to='/login' className='d-block'>Entrar</Link>
              </p>
            </Form>
          </Col>
        </Row>
      </Container>
    </Layout>
  )
}
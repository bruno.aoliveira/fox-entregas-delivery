import styled from 'styled-components';
import { Container } from 'react-bootstrap';
import bgMobile from '../../assets/bg-fox-entregas-mobile.jpg';
import bgDesktop from '../../assets/bg-fox-entregas.jpg';
import { Layout } from '../../components/Layout';
import { Button } from '../../components/Button';
import { useSelector } from 'react-redux';
import { selectUserLoggedIn } from '../../store/slices/userSlice';
import { Link } from "react-router-dom";

export const HomeView = () => {

  const isUserLoggedIn = useSelector(selectUserLoggedIn);


  return (
    <Layout startTransparent withoutMargin>
      <WrapStyled className='vh-100'>
        <Container className="h-100 d-flex flex-column  justify-content-center">
          <TitleStyled className='text-center text-lg-left text-white mt-auto mt-lg-0'>Fazemos sua entrega de forma rápida e barata</TitleStyled>
          <div className="mt-auto mt-lg-3 d-flex flex-column align-items-center align-items-lg-start mb-3">
            {isUserLoggedIn ? (
              <>
                <Button
                  variant='success'
                  size='lg'
                  to='/novo-pedido'
                  forwardedAs={Link}>
                  Fazer Pedido
                </Button>
              </>
            )
              : (
                <>
                  <Button variant='success'
                    size='lg'
                    to='/cadastro'
                    className='mb-2'
                    forwardedAs={Link}>
                    Criar conta
                  </Button>
                  <Button
                    variant='success'
                    size='lg'
                    to='/login'
                    forwardedAs={Link}>
                    Fazer Login
                  </Button>
                </>
              )}
          </div>
        </Container>
      </WrapStyled>
    </Layout>
  )
}

const WrapStyled = styled.div`
  background: url(${bgMobile}) no-repeat center center;
  background-size: cover;
  @media screen and (min-width: 576px) {
    background-image: url(${bgDesktop})
  }
  @media screen and (min-width: 768px) {
    background-image: url(${bgMobile})
  }
  @media screen and (min-width: 992px) {
    background-image: url(${bgDesktop})
  }
`

const TitleStyled = styled.h1`
  font-size: 2.25rem;
  text-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
  @media screen and (min-width: 992px) {
    font-size: 3rem;
    max-width: 500px;
  }
`

import { Container } from "react-bootstrap"
import { Layout } from "../../components/Layout"
import { PageTitle } from "../../components/PageTitle"

export const NotFound = () => {
    return (
        <Layout>
            <Container>
                <PageTitle>Página não encontrada x_x</PageTitle>
                <p>A página que você está tentando acessar não foi encontrada ou foi movida.</p>
                <p>Utilize o menu superior para encontrar o que deseja.</p>
            </Container>
        </Layout>
    )
}
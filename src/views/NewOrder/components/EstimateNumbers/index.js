import { useSelector } from "react-redux"
import styled from "styled-components";
import { selectCurrentEstimate } from "../../../../store/slices/estimateSlice"

export const EstimateNumbers = () => {

    const currentEstimate = useSelector(selectCurrentEstimate);

    return <WrapStyled>
        <NumberItemStyled>
            <span>Tempo</span>
            {currentEstimate.minutes} min
        </NumberItemStyled>
        <NumberItemStyled>
            <span>Distância</span>
            {(currentEstimate.meters / 1000).toLocaleString('pt-br', { maximumFractionDigits: 2 })} km
        </NumberItemStyled>
        <NumberItemStyled>
            <span>Valor</span>
            R$ {currentEstimate.value.toLocaleString('pt-br', { minimunFractionDigits: 2 })}
        </NumberItemStyled>
    </WrapStyled>
}

const WrapStyled = styled.div`
  background-color: #EAEAEA;
  padding: 15px 0;
  display: flex;
  justify-content: space-around;
`

const NumberItemStyled = styled.p`
  margin: 0;
  text-align: center;
  span {
    display: block;
    font-size: 0.875rem;
  }
`
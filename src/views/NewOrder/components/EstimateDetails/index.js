import { useSelector } from "react-redux";
import styled from "styled-components";
import { selectHasCurrentEstimate } from "../../../../store/slices/estimateSlice";
import { EstimateFinish } from "../EstimateFinish";
import { EstimateMap } from "../EstimateMap";
import { EstimateNumbers } from "../EstimateNumbers";

export const EstimateDetails = () => {

    const hasCurrentEstimate = useSelector(selectHasCurrentEstimate);


    if(!hasCurrentEstimate) {

        return (
            <WithoutEstimateStyled className="d-none d-md-flex">
                <p className="m-0">Preencha os campos ao lado para ver o preço</p>
            </WithoutEstimateStyled>
        )
    }

    return (
        <WithEstimateStyled>
            <EstimateMap />
            <EstimateNumbers />
            <EstimateFinish />
        </WithEstimateStyled>
    )
}


const WithoutEstimateStyled = styled.div`
    background-color: #EFEFEF;
    border: 1px dashed #000;
    display: flex;
    justify-content: center;
    align-items: center;
    height: 100%;
    padding: 0 100px;
    text-align: center;
`;

const WithEstimateStyled = styled.div`
    height: 100%;
    display: flex;
    flex-direction: column;
    
`;
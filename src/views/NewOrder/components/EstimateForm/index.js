import { useFormik } from "formik";
import { Alert, Form } from "react-bootstrap";
import { Button } from "../../../../components/Button";
import { FormField } from "../../../../components/FormField";
import * as yup from "yup";
import { AutocompleteField } from "../../../../components/AutoCompleteField";
import { createEstimate } from "../../../../services/createEstimate";
import { getErrorDescription } from "../../../../services/getErrorDescription"
import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { selectHasCurrentEstimate, selectCurrentEstimate, setCurrentEstimate, clearCurrentEstimate } from "../../../../store/slices/estimateSlice";

export const EstimateForm = () => {

    const dispatch = useDispatch();
    const hasCurrentEstimate = useSelector(selectHasCurrentEstimate)
    const currentEstimate = useSelector(selectCurrentEstimate)
    const [generalError, setGeneralError] = useState();
    const formik = useFormik({
        initialValues: {
            pickupAddress: currentEstimate?.pickupAddress || "",
            deliveryAddress: currentEstimate?.deliveryAddress || "",
            comments: currentEstimate?.comments || ""
        },
        validationSchema: yup.object().shape({
            pickupAddress: yup.object().required("Selecione o endereço na lista"),
            deliveryAddress: yup.object().required("Selecione o endereço na lista"),
            comments: yup.string().required("Informe as instruções"),

        }),
        onSubmit: async (values, { setFieldError }) => {

            setGeneralError(undefined)
            const { error, data } = await createEstimate(values)
            if (!error) {
                const action = setCurrentEstimate({
                    ...data,
                    ...values
                })
                dispatch(action)
                return;
            }
            if (data.msg !== "ValidationError") {
                setGeneralError("Ocorreu um erro ao calcular.")
                return;
            }
            data.errors.forEach(errorDetail => {
                const message = getErrorDescription(errorDetail.msg)
                setFieldError(errorDetail.param, message)
            })
        }
    })

    const getFieldProps = (fieldName) => ({
        ...formik.getFieldProps(fieldName),
        isValid: formik.touched[fieldName] && !formik.errors[fieldName],
        isInvalid: formik.touched[fieldName] && formik.errors[fieldName],
        error: formik.errors[fieldName]
    })

    const handleClear = () => {
        const action = clearCurrentEstimate();
        dispatch(action)
    }

    return (
        <>
            <Form onSubmit={formik.handleSubmit}>
                <AutocompleteField
                    {...getFieldProps("pickupAddress")}
                    onChange={address => formik.setFieldValue('pickupAddress', address)}
                    label="Endereço de retirada (A)"
                    placeholder="Informe o endereço completo"
                    disabled={hasCurrentEstimate} />
                <AutocompleteField
                    {...getFieldProps("deliveryAddress")}
                    onChange={address => formik.setFieldValue('deliveryAddress', address)}
                    label="Endereço de entrega (B)"
                    placeholder="Informe o endereço completo"
                    disabled={hasCurrentEstimate} />

                <FormField
                    {...getFieldProps("comments")}
                    label="Observações"
                    placeholder="Insira as observações para a entrega"
                    as="textarea"
                    disabled={hasCurrentEstimate} />
                {generalError && (
                    <Alert variant='danger'>{generalError}</Alert>
                )}
                {!hasCurrentEstimate && (
                    <Button type="submit"
                        block={window.innerWidth < 768}
                        loading={formik.isValidating || formik.isSubmitting}
                        disabled={formik.isValidating || formik.isSubmitting}
                    >Calcular preço</Button>
                )}
            </Form>
            {hasCurrentEstimate &&
                (
                    <Button
                        variant="outline-primary"
                        className="mb-3 mb-md-0"
                        block={window.innerWidth < 768}
                        onClick={handleClear}
                    >Alterar endereços</Button>
                )}
        </>
    )
}


/* global google */

import { GoogleMap, Marker } from "@react-google-maps/api"
import { useSelector } from "react-redux"
import styled from "styled-components";
import { LoadGoogleScript } from "../../../../components/LoadGoogleScript"
import { selectCurrentEstimate } from "../../../../store/slices/estimateSlice"
import  pinA  from "../../../../assets/pinA.svg";
import  pinB  from "../../../../assets/pinB.svg";

export const EstimateMap = () => {

    const currentEstimate = useSelector(selectCurrentEstimate);

    const handleLoad = map => {
        const bounds = new google.maps.LatLngBounds();
        bounds.extend({
            lat: currentEstimate.pickupAddress.lat,
            lng: currentEstimate.pickupAddress.lng
        })
        bounds.extend({
            lat: currentEstimate.deliveryAddress.lat,
            lng: currentEstimate.deliveryAddress.lng
        })
        const center = bounds.getCenter()
        map.setCenter(center)
        map.fitBounds(bounds)
    }

    return (
        <GoogleWrap>
            <LoadGoogleScript>
                <GoogleMap
                    center={{ lat: 0, lng: 0 }}
                    zoom={16}
                    mapContainerStyle={{
                        minHeight: 150,
                        flex: 1
                    }}
                    clickableIcons={false}
                    options={{
                        disablDefaultUI: true,
                        disableDoubleClickZoom: true,
                        draggable: false,
                        draggableCursor: "pointer"
                    }}
                    onLoad={handleLoad}
                >
                    <Marker
                        icon={pinA}
                        position={{
                            lat: currentEstimate.pickupAddress.lat,
                            lng: currentEstimate.pickupAddress.lng
                        }}
                    />
                    <Marker
                        icon={pinB}
                        position={{
                            lat: currentEstimate.deliveryAddress.lat,
                            lng: currentEstimate.deliveryAddress.lng
                        }}
                    />
                </GoogleMap>
            </LoadGoogleScript>
        </GoogleWrap>
    )
}

const GoogleWrap = styled.div`
    flex-grow: 1;
    display: flex;
    a[href^="http://maps.google.com/maps"]{display:none !important}
    a[href^="https://maps.google.com/maps"]{display:none !important}
    .gmnoprint a, .gmnoprint span, .gm-style-cc {
        display:none;
    }
    .gmnoprint div {
        background:none !important;
    }
`;
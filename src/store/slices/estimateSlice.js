import { createSlice } from "@reduxjs/toolkit";


const initialState = {
    currentEstimate: null,
}

export const slice = createSlice({
    name: "estimate",
    initialState,
    reducers: {
        setCurrentEstimate: (state, action) => {
            state.currentEstimate = action.payload;
        },
        clearCurrentEstimate: state => state.currentEstimate = null
    }
})

export const { setCurrentEstimate, clearCurrentEstimate } = slice.actions;

export default slice.reducer;

export const selectHasCurrentEstimate = state => !!state.estimate.currentEstimate;

export const selectCurrentEstimate = state => state.estimate.currentEstimate;

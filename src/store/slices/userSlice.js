import { createSlice } from '@reduxjs/toolkit'
import { getStorageItem } from '../../services/storage'

const user = JSON.parse(getStorageItem('user'))

const stateWithoutUser = {
  user: null,
  userLoggedIn: false
}
const initialState = user
  ? {
    user,
    userLoggedIn: true
  }
  : stateWithoutUser

const slice = createSlice({
  name: 'user',
  initialState,
  reducers: {
    updateUser: (state, action) => {
      state.userLoggedIn = true
      state.user = action.payload
    },
    deleteUser: state => state = stateWithoutUser
  }
})

export default slice.reducer;

export const { updateUser, deleteUser } = slice.actions;

export const selectUserLoggedIn = state => state.user.userLoggedIn;
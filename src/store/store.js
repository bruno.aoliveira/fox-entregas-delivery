import { configureStore } from '@reduxjs/toolkit'
import estimateSlice from './slices/estimateSlice'
import userReducer from './slices/userSlice'

export default configureStore({
  reducer: {
    user: userReducer,
    estimate: estimateSlice
  }
})
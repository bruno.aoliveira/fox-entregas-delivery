import { Header } from '../Header'
import { Footer } from '../Footer'
import styled from 'styled-components';


export const Layout = ({ children, startTransparent, withoutMargin }) => {
  return (
    <>
      <Header startTransparent={startTransparent}/>
      <WrapStyled startTransparent={startTransparent}>
        {children}
      </WrapStyled>
      <Footer withoutMargin={withoutMargin}/>
    </>
  )
}


const WrapStyled = styled.div`
  ${
    props => !props.startTransparent &&`
    height: 90vh;
      padding-top: 77px;
      @media screen and (min-width: 992px){
        padding-top: 96px;
      }
    `
  }
`;
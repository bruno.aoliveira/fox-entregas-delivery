import { useSelector } from "react-redux"
import { Route } from "react-router-dom";
import { Redirect } from "react-router-dom/cjs/react-router-dom.min";
import { selectUserLoggedIn } from "../../store/slices/userSlice"

export const PublicRouteOnly = ({ children, ...otherProps }) => {
  const isUserLoggedIn = useSelector(selectUserLoggedIn);
  return (
    <Route
      {...otherProps}
      render={() => !isUserLoggedIn ? children : <Redirect to={{ pathname: '/novo-pedido' }} />}
    />
  )
}
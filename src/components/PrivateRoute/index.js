import { Redirect, Route } from 'react-router-dom'
import { useSelector } from 'react-redux'
import { selectUserLoggedIn } from '../../store/slices/userSlice' 

export const PrivateRoute = ({ children, ...otherProps }) => {
  const isUserLoggedIn = useSelector(selectUserLoggedIn)
  return (
    <Route
      {...otherProps}
      render={() => isUserLoggedIn ? children : <Redirect to={{ pathname: '/login' }} /> }
    />
  )
}
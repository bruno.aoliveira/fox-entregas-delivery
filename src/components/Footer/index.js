import { faFacebookSquare, faInstagram } from "@fortawesome/free-brands-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Container, Nav } from "react-bootstrap";
import { useSelector } from "react-redux";
import styled from "styled-components";
import Logo from "../../assets/logo-fox-entregas.svg";
import { selectUserLoggedIn } from "../../store/slices/userSlice";
import { Link } from "react-router-dom";

export const Footer = ({ withoutMargin = false }) => {

  const isUserLoggedIn = useSelector(selectUserLoggedIn);


  return <FooterStyled className={`text-center ${withoutMargin ? "" : "mt-5"}`}>
    <Container className='d-lg-flex align-items-center'>
      <Link to="/">
        <img src={Logo} alt='Logo Fox Entregas' width={255} height={67} />
      </Link>
      <Nav className='flex-column flex-lg-row my-4 my-lg-0 ml-lg-auto'>
        <LinkStyled to="/" forwardedAs={Link}>Início</LinkStyled>
        {isUserLoggedIn ? (
          <>
            <LinkStyled to="/novo-pedido" forwardedAs={Link}>Novo Pedido</LinkStyled>
          </>
        ) : (
          <>
            <LinkStyled to="/cadastro" forwardedAs={Link}>Cadastro</LinkStyled>
            <LinkStyled to="/login" forwardedAs={Link}>Login</LinkStyled>
          </>)}
        <LinkStyled href="/termos-de-uso.pdf" target="_blank">Termos de Uso</LinkStyled>
      </Nav>
      <Nav className='justify-content-center'>
        <LinkStyled href='https://facebook.com' target="_blank" rel="noreferrer noopener" className='px-2'>
          <IconStyled icon={faFacebookSquare} />
        </LinkStyled>
        <LinkStyled href='https://instagram.com' target="_blank" rel="noreferrer noopener" className='px-2'>
          <IconStyled icon={faInstagram} />
        </LinkStyled>
      </Nav>
    </Container>
  </FooterStyled>
}

const FooterStyled = styled.footer`
  background-color: #414141;
  color: #FFF;
  padding: 30px 0 40px;
  @media screen and (min-width: 992px) {
    padding: 15px 0;
  }
`

const LinkStyled = styled(Nav.Link)`
  color: #FFF !important;
  &:hover {
    color: #CCC !important;
  }
`

const IconStyled = styled(FontAwesomeIcon)`
  font-size: 2.5rem;
`
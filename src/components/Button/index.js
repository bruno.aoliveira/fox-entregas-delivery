import styled from "styled-components";
import { Button as ButtonBootstrap, Spinner } from 'react-bootstrap';

export const Button = ({children, loading, ...otherProps}) => {
    return (
        <ButtonStyled {...otherProps}>
            {loading && (
                <Spinner animation='border' size='sm' role='status' className='mr-2'>
                    <span className='sr-only'>Carregando...</span>
                </Spinner>
            )}
            {children}
        </ButtonStyled>
    )
};

const ButtonStyled = styled(ButtonBootstrap)`
    border-radius: 100px;
    padding-left: 50px;
    padding-right: 50px;
    font-weight: 500;
    ${props => props.size === "lg" && `
        font-size: 1.125rem;
    `}
    ${props => (props.variant === 'primary' || !props.variant) && `
    background-color: #1117A3;
    border-color: #1117A3;
    &:hover {
      background-color: #2329BF;
      border-color: #2329BF;
    }
  `}
  ${props => props.variant === 'outline-primary' && `
    color: #1117A3;
    border-color: #1117A3;
    &:hover {
      background-color: #2329BF;
      border-color: #2329BF;
    }
`}`;
import { useLoadScript } from "@react-google-maps/api";
import { Alert, Spinner } from "react-bootstrap";


const libraries = ["places"];

export const LoadGoogleScript = ({ children }) => {

    const {isLoaded, loadError} = useLoadScript({
        googleMapsApiKey:"AIzaSyDKEDuNRTcXTAPDTvX8e9hfoPkrUaJu9dE",
        libraries
    })
    if(loadError) {
        return <Alert variant="danger">Falha ao carregar o Google. Recarregue a página.</Alert>  
    }
    if(!isLoaded) {
        return (
            <Spinner animation="grow" role="status" size="sm">
                <span className="sr-only">Carregando...</span>
            </Spinner>
        )
    }
    return children
    
}
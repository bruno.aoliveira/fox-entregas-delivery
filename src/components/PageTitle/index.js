import styled from "styled-components"

export const PageTitle = ({children}) => {
    return <TitleStyled className="text-center mt-4">{children}</TitleStyled>
}


const TitleStyled = styled.h1`
    color: #1117A3;
    font-size: 1.75rem;
    font-weight: 400;
`;
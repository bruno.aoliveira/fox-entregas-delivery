import { useEffect, useState } from "react";
import { Container, Nav, Navbar } from "react-bootstrap";
import Logo from "../../assets/logo-fox-entregas.svg";
import LogoBlue from "../../assets/logo-fox-entregas-azul.svg";
import styled from "styled-components";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faBars, faShippingFast, faSignInAlt, faSignOutAlt, faUser} from "@fortawesome/free-solid-svg-icons";
import { Button } from "../Button";
import { useDispatch, useSelector } from "react-redux";
import { deleteUser, selectUserLoggedIn } from "../../store/slices/userSlice";
import { Link, useHistory } from "react-router-dom";
import { logoutUser } from "../../services/logoutUser";
import { Button as ButtonBootstrap } from 'react-bootstrap';


export const Header = ({ startTransparent = false }) => {

  const dispatch = useDispatch();
  const history = useHistory();
  const isUserLoggedIn = useSelector(selectUserLoggedIn);
  const [isTransparent, setIsTransparent] = useState(startTransparent);

  useEffect(() => {
    const scrollChange = () => {
      const isLowScroll = window.scrollY < 50
      setIsTransparent(isLowScroll)
    }
    window.addEventListener('scroll', scrollChange)
    return () => {
      window.removeEventListener('scroll', scrollChange)
    }
  }, [isTransparent])

  const handleLogout = () => {
    logoutUser()
    dispatch(deleteUser())
    history.push('/login')
  }


  return (
    <NavbarStyled fixed='top' bg={isTransparent ? undefined : 'white'} expand="lg">
      <Container>
        <Navbar.Brand to="/" as={Link}>
          <ImageStyled src={isTransparent ? Logo : LogoBlue} alt='Logo Fox Entregas' widht={194} height={51} />
        </Navbar.Brand>
        <ToggleStyled>
          <FontAwesomeIcon icon={faBars} size='lg' className={isTransparent ? 'text-white' : 'text-dark'} />
        </ToggleStyled>
        <CollapseStyled className='justify-content-center text-center justify-content-lg-end'>
          <Nav>
            <NavLinkStyled isTransparent={isTransparent} to='/' forwardedAs={Link}>Início</NavLinkStyled>
            {isUserLoggedIn ? (
              <>
                <ButtonHeader className='mt-2 mt-lg-0 ml-lg-4' to='/novo-pedido' forwardedAs={Link}>
                  <IconStyled icon={faShippingFast} />
                  <TextStyled>Fazer pedido</TextStyled>
                </ButtonHeader>

                <ButtonHeader className='mt-2 mt-lg-0 ml-lg-4' onClick={handleLogout}>
                  <IconStyled icon={faSignOutAlt} />
                  <TextStyled>Sair</TextStyled>
                </ButtonHeader>
              </>
            ) : (
              <>
                <ButtonHeader className='mt-2 mt-lg-0 ml-lg-4' to='/cadastro' forwardedAs={Link}>
                  <IconStyled icon={faUser} />
                  Criar conta
                </ButtonHeader>

                <ButtonHeader className='mt-2 mt-lg-0 ml-lg-4' to='/login' forwardedAs={Link}>
                  <IconStyled icon={faSignInAlt} />
                  <TextStyled>Fazer login</TextStyled>
                </ButtonHeader>
              </>
            )}
          </Nav>
        </CollapseStyled>
      </Container>
    </NavbarStyled>
  )
}

const NavbarStyled = styled(Navbar)`
  transition: all .3s linear;
  ${props => props.bg === 'white' && `
    box-shadow: 0px 2px 4px rgba(0, 0, 0, 0.25);
  `}
`

const ImageStyled = styled.img`
  @media screen and (min-width: 992px) {
    width: 266px;
    height: auto;
  }
`


const ToggleStyled = styled(Navbar.Toggle)`
  border: none;
`

const CollapseStyled = styled(Navbar.Collapse)`
  @media screen and (max-width: 991px) {
    background: #FFF;
    margin: 0 -1rem;
    padding: 1rem 2rem;
  }
`

const NavLinkStyled = styled(Nav.Link)`
  color: #343a40 !important;
  @media screen and (min-width: 992px) {
    color: ${props => props.isTransparent ? '#FFF' : '#343a40'} !important;
  }
`


const ButtonHeader = styled(ButtonBootstrap)`
    display: flex;
    justify-content: space-between;
    width: 200px;
    border-radius: 100px;
    padding-left: 30px;
    padding-right: 30px;
    font-weight: 500;
    ${props => props.size === "lg" && `
        font-size: 1.125rem;
    `}
    ${props => (props.variant === 'primary' || !props.variant) && `
    background-color: #1117A3;
    border-color: #1117A3;
    &:hover {
      background-color: #2329BF;
      border-color: #2329BF;
    }
  `}
  ${props => props.variant === 'outline-primary' && `
    color: #1117A3;
    border-color: #1117A3;
    &:hover {
      background-color: #2329BF;
      border-color: #2329BF;
    }
`}`;


const IconStyled = styled(FontAwesomeIcon)`
  font-size: 1.3rem;

`

const TextStyled = styled.span`
`;



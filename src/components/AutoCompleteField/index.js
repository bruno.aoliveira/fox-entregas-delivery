/* global google */
import { Autocomplete } from "@react-google-maps/api"
import { useRef } from "react"
import { FormField } from "../FormField"
import { LoadGoogleScript } from "../LoadGoogleScript"

export const AutocompleteField = ({ value, onChange, ...fieldProps }) => {
  const autocompleteRef = useRef(null)
  const handleLoad = autocomplete => {
    autocompleteRef.current = autocomplete
  }
  const handleChange = () => {
    const place = autocompleteRef.current.getPlace()
    if (place.formatted_address) {
      const address = {
        lat: place.geometry.location.lat(),
        lng: place.geometry.location.lng(),
        address: place.formatted_address
      }
      onChange(address)
    }
  }
  return (
    <LoadGoogleScript>
      <Autocomplete
        onLoad={handleLoad}
        onPlaceChanged={handleChange}
        restrictions={{
          country: 'br'
        }}
        bounds={typeof google !== 'undefined' ? new google.maps.LatLngBounds(
          new google.maps.LatLng(-22.98694016055838, -43.21588199782191),
          new google.maps.LatLng(-22.95035116763602, -43.176142388450835)
        ) : undefined}
      >
        <FormField
          {...fieldProps}
          defaultValue={value?.address || ''}
        />
      </Autocomplete>
    </LoadGoogleScript>
  )
}

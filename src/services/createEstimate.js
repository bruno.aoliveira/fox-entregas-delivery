import { api } from "./api"

export const createEstimate = async (estimateData) => {
  try {
    const response = await api.post("/estimates", estimateData)
    return {
      error: false,
      data: response.data
    }
  } catch (error) {
    return {
      error: true,
      data: error.response?.data || "Erro inesperado."
    }
  }
}
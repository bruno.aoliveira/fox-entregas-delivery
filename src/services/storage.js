const prefix = '@foxentregas/'

export const setStorageItem = (key, value) => {
  localStorage.setItem(`${prefix}${key}`, value)
}

export const getStorageItem = key => {
  return localStorage.getItem(`${prefix}${key}`)
}

export const removeStorageItem = key => {
  localStorage.removeItem(`${prefix}${key}`)
}
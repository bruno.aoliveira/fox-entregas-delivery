export const getErrorDescription = key => {
    switch (key) {
      case 'should_be_a_valid_mobile_phone_number':
        return 'Informe um telefone válido.'
      case 'email_duplicated':
        return 'Este e-mail já foi utilizado.'
      default:
        return 'Campo inválido.'
    }
  }
  
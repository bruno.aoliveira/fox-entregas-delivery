import { api } from "./api"
import { setStorageItem } from "./storage"

export const loginUser = async (credentials) => {
  try {
    const response = await api.post('/login', {
      ...credentials,
      isPartnerLogin: false
    })
    setStorageItem('user', JSON.stringify(response.data))
    api.defaults.headers['Authorization'] = `Bearer ${response.data.accessToken}`
    return {
      error: false,
      data: response.data
    }
  } catch (error) {
    return {
      error: true,
      data: error.response?.data || 'Erro inesperado.'
    }
  }
}
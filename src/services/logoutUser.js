import { api } from './api'
import { removeStorageItem } from './storage'

export const logoutUser = () => {
  removeStorageItem('user')
  api.defaults.headers['Authorization'] = undefined
}

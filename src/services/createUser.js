import axios from "axios";
import { api } from "./api";

export const createUser = async (newUser) => {

    try{
        
        const response = await api.post("/users", newUser);
        return {
            error: false,
            data: response.data
        }
        
    } catch(error){
        return {
            error: true,
            data: error.response?.data || "Erro inesperado!"
        }
    }
}